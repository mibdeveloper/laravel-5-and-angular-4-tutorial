# Laravel 5.4 with Angular 4.3.4(Using Angular cli)

## Installation

```
composer install    (консоль OpenServer)
npm install         (консоль NodeJS)
```

Create `.env` file (can be based on `.env.example`)
`Надо настроить подключение к базе.`
```
php artisan key:generate (консоль OpenServer)
```

## Build

```
ng build (консоль NodeJS)
```

## Run Laravel - не обязательно. Есть OpenServer

```
php artisan serve
```

## Настройка OpenServer
Разместил проект в c:\OS3\domains\laravel5Angular4\
Надо перенастроить сервер. И пробросить корневую папку в public. http://prntscr.com/i1vlcb
После этого проект заработает нормально.

Для того, чтоб перестроить Frontend:

```
ng build (консоль NodeJS)
```
После построения js файлы будут помещены в `public/js`. Основная View его подключает. Дальше, дело за Angular


##Запуск проекта
```
ng build --watch
```

После этого проект будет перестраиваться каждый раз при изменении файлов Frontend 
## Добавление таблицы Героев и заполнение данными

Создаем миграцию: 

```
php artisan make:migration create_heroes_table
php artisan migrate
```

Создание сидеров:

```
php artisan make:seeder HeroesTableSeeder
```

Выполнение сида:
```
php artisan db:seed --class=HeroesTableSeeder
```
Выполняем не все сиды, а только для 'Героев'.


## Создание модели и контроллера для Героев

```
php artisan make:model Hero
php artisan make:controller HeroesController
```