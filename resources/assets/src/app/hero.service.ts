import { Injectable } from '@angular/core';
import {  Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

// import { Hero } from './hero';
 import { HEROES } from './mock-heroes';
 import { of } from 'rxjs/observable/of';

 import { MessageService } from './message.service';
import {Hero} from './hero';

@Injectable()
export class HeroService {

  constructor(private messageService: MessageService, private http: Http) { }

  /*getHeroes(): Hero[] {
    return HEROES;
  }*/

  /*
  getHeroes(): Observable<Hero[]> {
    this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }*/

    getHeroes(): Observable<Hero[]> {
      return this.http.get('api/heroes').map(responce => {
          return responce.json()['heroes'];
      });
  }


  getHero(id: number): Observable<Hero> {
    // Todo: send the message _after_ fetching the hero
    this.messageService.add(`HeroService: fetched hero id=${id}`);
    return of(HEROES[0]);

    /*of(HEROES.find(hero => hero.id === id));*/
  }
}
