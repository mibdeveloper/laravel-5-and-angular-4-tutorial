<?php
namespace App\Http\Controllers;


use App\Hero;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;





class HeroesController extends Controller
{
    public function getList()
    {
        $heroes = Hero::get();

        return response(compact('heroes'), 200);

        //return response()->success(compact('heroes'));
    }
}
