<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HeroesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for($i = 0; $i < 10; $i++) {
            $dt = Carbon::create(rand(2012,2017), rand(1,12), rand(1,28), rand(1,12));
            DB::table('heroes')->insert([
                'name'        => str_random(10),
                'created_at' => $dt,
                'updated_at' => $dt,
                //'email' => str_random(10).'@gmail.com',
                //'password' => bcrypt('secret'),
            ]);
        }
    }
}
